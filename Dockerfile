FROM node:12

WORKDIR /app

COPY ["package.json", "package-lock.json*", "./"]

RUN npm -g install serve
RUN yarn install

COPY . .
CMD serve -s build -l 4000
