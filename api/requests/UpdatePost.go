package requests

import (
	"context"
	"encoding/json"
	"errors"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"net/http"
)

type BoolResult struct {
	Result bool `json:"result"`
}

func UpdatePost(req *http.Request) (interface{}, error) {
	var reqData bson.M
	ctx := context.TODO()

	if (reqData["token"] != "YZqnjYdfiCfHxBojmkiX") {
		return nil, errors.New("Empty Token")
	}

	reqData["token"] = nil

	err := json.NewDecoder(req.Body).Decode(&reqData)
	CheckErr(err)

	collection := Client.Database("DB").Collection("Posts")
	id := reqData["id"].(string)
	reqData["id"] = nil

	if reqData["photo"] != nil {
		photoUrl, err := CreateImage(id, reqData["photo"].(string))
		if err != nil {
			return nil, err
		}
		reqData["photo"] = photoUrl
	}

	objId, err := primitive.ObjectIDFromHex(id)

	_, err = collection.UpdateOne(ctx, bson.M{"_id": objId}, bson.D{{"$set", reqData}})
	if err != nil {
		return nil, err
	}

	return BoolResult{Result: true}, nil

}
