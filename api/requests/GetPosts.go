package requests

import (
	"context"
	"encoding/json"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
	"math"
	"net/http"
	"time"
)

type PostQuery struct {
	Id         string    `bson:"_id" json:"id"`
	Title      string    `bson:"title" json:"title"`
	Content    string    `bson:"content" json:"content"`
	Category   string    `bson:"category" json:"category"`
	Starred    bool      `bson:"starred" json:"starred"`
	Annotation string    `bson:"annotation" json:"annotation"`
	AuthorId   int       `bson:"author_id" json:"author_id"`
	TimeAdded  time.Time `bson:"time_added" json:"time_added"`
	Photo      string    `bson:"photo" json:"photo"`
}

func GetPosts(req *http.Request) (interface{}, error) {
	var reqData bson.M
	err := json.NewDecoder(req.Body).Decode(&reqData)

	if err != nil {
		reqData = bson.M{}
	}
	reqData["starred"] = false

	ctx := context.TODO()
	collection := Client.Database("DB").Collection("Posts")

	first := reqData["first"]
	skip := reqData["skip"]

	reqData["first"] = nil
	reqData["skip"] = nil

	if (reqData["id"] != nil) {
		var data PostQuery
		id := reqData["id"].(string)
		objId, err := primitive.ObjectIDFromHex(id)
		CheckErr(err)
		err = collection.FindOne(ctx, bson.M{"_id": objId}).Decode(&data)
		CheckErr(err)
		return data, err
	}

	options := options.Find().SetSort(bson.M{"time_added": -1})
	cursor, err := collection.Find(ctx, reqData, options)
	CheckErr(err)
	defer cursor.Close(ctx)
	var results []PostQuery
	err = cursor.All(ctx, &results)

	if (first != nil || skip != nil) {
		left := int(math.Min(skip.(float64), float64(len(results)-1)))
		right := int(math.Min(skip.(float64)+first.(float64), float64(len(results)-1)))

		results = results[left:right]
	}

	CheckErr(err)
	return results, err
}

func GetStarredPosts(req *http.Request) (interface{}, error) {
	var reqData bson.M
	err := json.NewDecoder(req.Body).Decode(&reqData)

	if err != nil {
		reqData = bson.M{}
	}

	reqData["starred"] = true

	ctx := context.TODO()
	collection := Client.Database("DB").Collection("Posts")

	cursor, err := collection.Find(ctx, reqData)
	CheckErr(err)
	defer cursor.Close(ctx)
	var results []PostQuery
	err = cursor.All(ctx, &results)
	CheckErr(err)
	return results, err
}