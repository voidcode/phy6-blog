package requests

import (
	"context"
	"encoding/json"
	"fmt"
	"errors"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"net/http"
	"time"
)

func CreatePost(req *http.Request) (interface{}, error) {
	var reqData NewPost
	ctx := context.TODO()

	err := json.NewDecoder(req.Body).Decode(&reqData)
	CheckErr(err)

	collection := Client.Database("DB").Collection("Posts")

	if (reqData.Token != "YZqnjYdfiCfHxBojmkiX") {
		return nil, errors.New("Empty Token")
	}




	// 	if SearchToken(reqData.Token) {
	checkResult, checkError := postChecker(reqData)
	if checkResult {
		result, err := collection.InsertOne(
			ctx,
			bson.D{{"title", reqData.Title},
				{"content", reqData.Content},
				{"category", reqData.Category},
				{"starred", reqData.Starred},
				{"annotation", reqData.Annotation},
				{"author_id", reqData.AuthorId},
				{"time_added", time.Now()},
			},
		)
		CheckErr(err)
		fmt.Println(result)

		id := result.InsertedID.(primitive.ObjectID)

		if (reqData.PhotoBase64 != "") {
					photo, err := CreateImage(id.String()[10:34], reqData.PhotoBase64)
					if err != nil {
						return nil, err
					}

					_, err = collection.UpdateOne(ctx,
						bson.D{
							{"_id", id},
						},
						bson.D{{"$set", bson.D{{"photo", photo}}}},
					)

					if err != nil {
						return nil, err
					}
		}



		return BoolResult{Result: true}, err
	} else {
		CheckErr(err)
		return checkError, err
	}
	// 	}else{
	// 		return ErrorResult{"Token is not valid"},err
	// 	}
}

func postChecker(postData NewPost) (bool, ErrorResult) {
	if postData.Title == "" {
		return false, ErrorResult{"Not Title"}
	} else if postData.Content == "" {
		return false, ErrorResult{"NULL Content"}
	} else {
		return true, ErrorResult{"correct"}
	}
}
