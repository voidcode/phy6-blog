package requests

import (
	"bytes"
	"encoding/base64"
	"errors"
	"net/http"
	"strings"
)

// Creates URL by base64 string
func CreateImage(id string, encodedPhoto string) (string, error) {
	url := "https://storage.yandexcloud.net/phy-6/" + id + "/preview.png"
	client := &http.Client{}

	i := strings.Index(encodedPhoto, ",")
	if i < 0 {
		//log.Fatal("no comma in base64")
		return url, errors.New("No comma in base64")
	}

	decoder := base64.NewDecoder(base64.StdEncoding, strings.NewReader(encodedPhoto[i+1:]))

	buffer := bytes.Buffer{}
	_, err := buffer.ReadFrom(decoder)
	if err != nil {
		return url, err
	}

	request, err := http.NewRequest(http.MethodPut, url, bytes.NewReader(buffer.Bytes()))
	if err != nil {
		return url, err
	}

	_, err = client.Do(request)
	if err != nil {
		return url, err
	}

	return url, nil

}
