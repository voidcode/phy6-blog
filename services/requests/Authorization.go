package requests

import (
	"encoding/json"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"net/http"
	"time"
	/*"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"*/)

const TOKENLEN = 16

type AuthData struct {
	Login    string `json:"login" bson:"login"`
	Password string `json:"password" bson:"password"`
	Token    string `json:"token" bson:"token"`
}

type AuthDataInDB struct {
	id        string    `json:"id" bson:"id"`
	Login     string    `json:"login" bson:"login"`
	Password  string    `json:"password" bson:"password"`
	Token     string    `json:"token" bson:"token"`
	ValidTime time.Time `json:"valid_time" bson:"valid_time"`
}

func Authorization(req *http.Request) (interface{}, error) {
	var reqData AuthData

	err := json.NewDecoder(req.Body).Decode(&reqData)
	CheckErr(err)

	if SearchToken(reqData.Token) {
		return TokenResult{reqData.Token}, err
	} else {
		tokenGenerated := GenerateToken(TOKENLEN)
		collection := Client.Database("DB").Collection("UserAuth")

		if searchEnterInDB(reqData) {
			result, err := collection.UpdateOne(
				Ctx,
				bson.D{{"login", reqData.Login}},
				bson.D{
					{"$set", bson.D{{"token", tokenGenerated}, {"valid_time", time.Now()}}},
				})
			CheckErr(err)
			fmt.Println(result)
			return ErrorResult{"Complete"}, err
		} else {
			res, err := collection.InsertOne(Ctx, bson.D{{"login", reqData.Login}, {"password", reqData.Password},
				{"token", tokenGenerated}, {"valid_time", time.Now()}})
			CheckErr(err)
			fmt.Println(res)
			return ErrorResult{"Ok"}, err
		}
	}
}

func searchEnterInDB(userData AuthData) bool {
	var dataInDB AuthDataInDB

	collection := Client.Database("DB").Collection("UserAuth")

	checker := collection.FindOne(Ctx, bson.D{{"login", userData.Login}, {"password", userData.Password}}).Decode(&dataInDB)
	fmt.Println(userData.Password, dataInDB.Password, checker)
	if checker != nil {
		return false
	} else if dataInDB.Password == userData.Password {
		return true
	} else {
		return false
	}
}
