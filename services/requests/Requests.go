package requests

import (
	"encoding/json"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"math/rand"
	"net/http"
	"os"
	"time"
)

var Client, Ctx, Cancel, err = Connect(os.Getenv("DB_URL"))

type ErrorResult struct {
	Error string `json:"error"`
}

type TokenResult struct {
	Token string `json:"token"`
}

type NewPost struct {
	Title       string    `json:"title"`
	Content     string    `json:"content"`
	Category    string    `json:"category"`
	Starred     bool      `json:"starred"`
	Annotation  string    `json:"annotation"`
	AuthorId    int       `json:"author_id"`
	Token       string    `json:"token"`
	TimeAdded   time.Time `json:"time_added"`
	PhotoBase64 string    `json:"photo"`
}

var LetterRunes = []rune("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func GenerateToken(length int) string {
	b := make([]rune, length)
	for i := range b {
		b[i] = LetterRunes[rand.Intn(len(LetterRunes))]
	}
	return string(b)
}

func RequestHandler(request func(req *http.Request) (interface{}, error)) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, req *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Content-Type", "application/json")

		result, err := request(req)

		if err != nil {
			fmt.Println(err)
			result := ErrorResult{Error: err.Error()}
			jsonData, _ := json.Marshal(result)
			w.Write(jsonData)
			return
		}

		jsonData, err := json.Marshal(result)
		w.Write(jsonData)
	}
}

func SearchToken(token string) bool {
	var data AuthDataInDB

	collection := Client.Database("DB").Collection("UserAuth")

	err := collection.FindOne(Ctx, bson.D{{"token", token}}).Decode(&data)
	if err != nil {
		return false
	} else {
		return TimeCheck(data)
	}
	defer Cancel()

	return false
}

func TimeCheck(data AuthDataInDB) bool {
	if data.ValidTime.Before(time.Now()) {
		check := time.Now().AddDate(0, 0, 7)
		if data.ValidTime.After(check) {
			return false
		}
	}
	return true
}

func CheckErr(err error) {
	if err != nil {
		panic(err)
	}
}
