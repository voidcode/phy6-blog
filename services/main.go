package main

import (
	"fmt"
	"net/http"
	"os"

	"voidcode.ru/phy6-blog/api/requests"

	/*"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"*/
)

func headers(w http.ResponseWriter, req *http.Request) {
	for name, headers := range req.Header {
		for _, h := range headers {
			fmt.Fprintf(w, "%v: %v\n", name, h)
		}
	}
}

func main() {

	var Handler = requests.RequestHandler

	var prefix = os.Getenv("PREFIX")

	requests.Ping(requests.Client, requests.Ctx)

	http.HandleFunc(prefix+"/authorization", Handler(requests.Authorization))
	http.HandleFunc(prefix+"/create_post", Handler(requests.CreatePost))
	http.HandleFunc(prefix+"/get_posts", Handler(requests.GetPosts))
	http.HandleFunc(prefix+"/get_starred_posts", Handler(requests.GetStarredPosts))
	http.HandleFunc(prefix+"/update_post", Handler(requests.UpdatePost))
	http.HandleFunc(prefix+"/headers", headers)

	fmt.Println("Serving at " + "URL" + prefix + "/query-name")
	portStr := os.Getenv("PORT")
	fmt.Println("Listening on " + portStr)
	err := http.ListenAndServe(":"+portStr, nil)
	checkErr(err)

}

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}
