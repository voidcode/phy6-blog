import React, { useEffect, useState } from "react";
import colors from "../../globals/colors";
import { cat, menu, arrowBack } from "../../globals/images";
import HrefBtn from "./hrefBtn";
import SearchBar from "../SearchBar";
import { useHistory } from "react-router-dom";

import "./index.css";

export default ({ selected, ...props }) => {
  const mobileRender = () =>
    ["Главная", "Мемы", "Блог", "О нас"].map((text, index) => {
      switch (text) {
        case "Главная":
          return (
            <HrefBtn
              key={text}
              text={text}
              className={"text btnText mobileBtn"}
              style={text === selected ? { color: "#BEA322" } : {}}
              onClick={() => navigateWithReset("/")}
            />
          );
        case "Мемы":
          return (
            <HrefBtn
              key={text}
              text={text}
              className={"text btnText mobileBtn"}
              style={text === selected ? { color: "#BEA322" } : {}}
              onClick={() => navigateWithReset("/memes")}
            />
          );

        case "Блог":
          return (
            <HrefBtn
              key={text}
              text={text}
              className={"text btnText mobileBtn"}
              style={text === selected ? { color: "#BEA322" } : {}}
              onClick={() => navigateWithReset("/blog")}
            />
          );
        case "О нас":
          return (
            <HrefBtn
              key={text}
              text={text}
              className={"text btnText mobileBtn"}
              style={text === selected ? { color: "#BEA322" } : {}}
              onClick={() => navigateWithReset("/aboutUs")}
            />
          );

        default:
          return (
            <HrefBtn
              key={text}
              text={text}
              className={"text btnText"}
              style={text === selected ? { color: "#BEA322" } : {}}
              onClick={() => navigateWithReset("/")}
            />
          );
      }
    });

  const desktopRender = () =>
    ["Главная", "Мемы", "Блог", "О нас"].map((text, index) => {
      switch (text) {
        case "Главная":
          return (
            <HrefBtn
              key={text}
              text={text}
              className={"text btnText"}
              style={text === selected ? { color: "#BEA322" } : {}}
              onClick={() => navigateWithReset("/")}
            />
          );
        case "Мемы":
          return (
            <HrefBtn
              key={text}
              text={text}
              className={"text btnText"}
              style={text === selected ? { color: "#BEA322" } : {}}
              onClick={() => navigateWithReset("/memes")}
            />
          );

        case "Блог":
          return (
            <HrefBtn
              key={text}
              text={text}
              className={"text btnText"}
              style={text === selected ? { color: "#BEA322" } : {}}
              onClick={() => navigateWithReset("/blog")}
            />
          );
        case "О нас":
          return (
            <HrefBtn
              key={text}
              text={text}
              className={"text btnText"}
              style={text === selected ? { color: "#BEA322" } : {}}
              onClick={() => navigateWithReset("/aboutUs")}
            />
          );

        default:
          return (
            <HrefBtn
              key={text}
              text={text}
              className={"text btnText"}
              style={text === selected ? { color: "#BEA322" } : {}}
              onClick={() => navigateWithReset("/")}
            />
          );
      }
    });

  const [isMenuOpened, setMenuOpened] = useState(0);

  const [width, setWidth] = useState(0);
  useEffect(() => {
    setWidth(window.innerWidth);
    window.addEventListener("resize", (event) => {
      setWidth(window.innerWidth);
    });
  }, []);

  const navigateWithReset = (route) => {
    sessionStorage.setItem("scrollPosition", 0);
    navigation.push(route);
  };

  const navigation = useHistory();
  if (width > 500)
    return (
      <div id="headerFiller">
        <div className={"root"}>
          <div className={"halfSplit"} id={"logoRow"}>
            {/* Cat Logo */}
            <img
              src={cat}
              className={"logo"}
              alt="logo"
              onClick={() => navigateWithReset("/")}
            />
            {/* Text Logo*/}
            <div
              onClick={() => navigateWithReset("/")}
              style={{
                textAlign: "center",
                color: colors.mainText,
                display: "flex",
                flexDirection: "row",
                margin: 10,
              }}
            >
              <div className="text" id="phyText">
                Phy
              </div>
              <div className="text" id="sixText">
                6
              </div>
            </div>

            {/* <div style={{marginLeft:'6%', flex: 1,height:'70%', maxWidth:300, justifyContent:'center',}}><SearchBar /></div> */}
          </div>
          <div className={"halfSplit"} id="btnRow">
            <div className={"btnRow"}>{desktopRender()}</div>
          </div>
        </div>
      </div>
    );
  else
    return (
      <div id="headerMobile">
        <div id="headerMobileFiller">
          <div id="headerMenu" className={isMenuOpened ? "show" : "hide"}>
            <div id="menuBody">
              <div
                onClick={() => {
                  setMenuOpened(!isMenuOpened);
                }}
              >
                <img src={cat} className="mobileCat"></img>
              </div>
              {mobileRender()}
            </div>
            <div
              style={{
                width: "20%",
                height: "100%",
                background: "white",
                opacity: 0,
              }}
              onClick={() => {
                setMenuOpened(!isMenuOpened);
              }}
            ></div>
          </div>
          <div id="headerMobileContainer">
            <div
              id="headerMobileMenuIcon"
              onClick={() => {
                selected !== "Пост"
                  ? setMenuOpened(!isMenuOpened)
                  : navigation.goBack();
              }}
            >
              {selected !== "Пост" ? (
                <img
                  src={menu}
                  alt="Картинка в отпуске!"
                  id="headerMobileMenuIcon"
                ></img>
              ) : (
                <img
                  src={arrowBack}
                  alt="Картинка в отпуске!"
                  id="headerMobileArrowIcon"
                ></img>
              )}
            </div>
            <div id="mobileTitle">{selected}</div>
          </div>
        </div>
      </div>
    );
};
