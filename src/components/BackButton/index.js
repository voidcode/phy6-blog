import React, { useState, useEffect } from "react";
import { arrowBackDesktop } from "../../globals/images";

import "./index.css";
import { useHistory } from "react-router-dom";

const BackButton = () => {
  const [isShowingButton, setIsShowingButton] = useState(0);

  const [width, setWidth] = useState(0);

  const navigation = useHistory();
  let category = sessionStorage.getItem("category");
  if (category === "undefined") {
    category = undefined;
  }

  useEffect(() => {
    setWidth(window.innerWidth);
    window.addEventListener("resize", (event) => {
      setWidth(window.innerWidth);
    });
  }, []);

  if (width < 750) return null;
  return (
    <div
      id="backButtonMainContainer"
      onClick={() =>
        category
          ? navigation.push("/category/" + category)
          : navigation.push("/")
      }
    >
      <div id="backButtonSliding"></div>
      <div id="backButtonArrowContainer">
        <img
          src={arrowBackDesktop}
          id="backButtonArrow"
          alt="Стрелочка за границей("
        ></img>
      </div>
    </div>
  );
};
export default BackButton;
