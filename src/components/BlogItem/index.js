import React, { useState, useEffect } from "react";
import { URL } from "../../globals/index";
import "./index.css";

import firebase from "firebase";
import "firebase/storage";
import { useHistory } from "react-router-dom";

const BlogItem = ({
  ref,
  onClick,
  imgProps,
  imageUrl,
  textProps,
  ...props
}) => {
  const [image, setImage] = useState("");
  // const storage = firebase.storage();

  useEffect(() => {
    // getImage(imageId);
  }, []);

  const getImage = async (path) => {
    const body = { id: path };
    await fetch(URL + "/getImage", {
      method: "POST",
      body: JSON.stringify(body),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setImage(data.result);
      });
    // await storage
    //   .ref()
    //   .child("/images/" + path + "/preview.png")
    //   .getDownloadURL()
    //   .then((url) => {
    //     setImage(url);
    //   });
  };

  const navigation = useHistory();

  return (
    <div className="blogItem" ref={ref} {...props}>
      <div id="postContainer">
        {/* Превьюшка поста */}
        <img
          onClick={onClick}
          id="postImage"
          src={imageUrl}
          alt="Нет картинки("
          {...{ ...defaultImgProps, ...imgProps }}
        />

        {/* Заголовок */}
        {props.title && (
          <div onClick={onClick} id="postTitle">
            {props.title}
          </div>
        )}

        {/* Аннотация */}
        <div id="postContent" {...textProps}>
          {props.text || ""}
        </div>

        <div id="postInfo">
          <div
            id="postCategory"
            onClick={() => {
              navigation.push("/category/" + props.category || defaultCategory);
            }}
          >
            {props.category || defaultCategory}
          </div>
          <div id="postDate">{props.date || defaultDate}</div>
        </div>
      </div>
    </div>
  );
};

const defaultCategory = "Без категории";
const defaultDate = "Без даты";
const defaultImgProps = {
  // style: {}
};

export default BlogItem;
