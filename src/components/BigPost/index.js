import React, { useState, useEffect } from "react";
import "./index.css";
import firebase from "firebase";
import "firebase/storage";
import { URL } from "../../globals/index";
import BlogItem from "../BlogItem";
import { useHistory } from "react-router-dom";

const BigPost = ({
  imageUrl,
  onClick,
  title = "",
  annotation = "",
  category = "",
  date = "",
  ...props
}) => {
  const [image, setImage] = useState("");
  // const storage = firebase.storage();

  useEffect(() => {
    // getImage(imageId);
  }, []);

  const getImage = async (path) => {
    const body = { id: path };
    await fetch(URL + "/getImage", {
      method: "POST",
      body: JSON.stringify(body),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setImage(data.result);
      });
  };

  const [width, setWidth] = useState(0);
  const navigation = useHistory();

  useEffect(() => {
    setWidth(window.innerWidth);
    window.addEventListener("resize", (event) => {
      setWidth(window.innerWidth);
    });
  }, []);

  if (width > 500)
    return (
      <div {...props} id="bigPostRoot">
        <div id="bigPostContainer">
          <img
            onClick={onClick}
            id="bigPostImage"
            src={imageUrl}
            alt="Картинки нет или идет загрузка("
          />
          <div id="bigPostTextContainer">
            <div id="bigPostTitle" onClick={onClick}>
              {title}
            </div>
            <div id="bigPostAnnotation">{annotation}</div>
            <div id="bigPostInfo">
              <div
                id="bigPostCategory"
                onClick={() => {
                  navigation.push("/category/" + category);
                }}
              >
                {category}
              </div>
              <div id="bigPostDate">{date}</div>
            </div>
          </div>
        </div>
      </div>
    );
  else
    return (
      <BlogItem
        text={annotation}
        {...{
          imageUrl,
          onClick,
          title,
          annotation,
          category,
          date,
          ...props,
        }}
      />
    );
};

export default BigPost;
