import React from "react";

export default () => (
  <input
    placeholder="Поиск"
    style={{
      background: "rgba(184, 184, 184, 0.5)",
      borderRadius: "5px",
      height: "100%",
      width: "100%",
      alignItems: "center",
      boxSizing: "border-box",
    }}
  ></input>
);
