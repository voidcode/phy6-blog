import React, { useState, useEffect } from "react";
import "./index.css";
import { vkLogo, tgLogo } from "../../globals/images";
import { useHistory } from "react-router-dom";

export default () => {
  const history = useHistory();
  const [width, setWidth] = useState(0);

  useEffect(() => {
    setWidth(window.innerWidth);
    window.addEventListener("resize", (event) => {
      setWidth(window.innerWidth);
    });
  }, []);

  if (width > 500)
    return (
      <div
        style={{
          // marginTop: "64px",
          // height: "20%",
          background: "#191919",
        }}
      >
        <div id="footerFiller">
          <div id="footerGrid">
            <div className="footerText">
              <div className="footerTitle">Ссылки:</div>
              <div
                className="link"
                onClick={() => {
                  history.push("/licenseAgreement");
                }}
              >
                Пользовательское соглашение
              </div>
              {/* <div className="link">Обратная связь</div> */}
            </div>
            <div className="footerText" id="contacts">
              <div className="footerTitle">Контакты:</div>
              <div
                className="clickable footerMarginLink"
                onClick={() => {
                  window.location = "mailto:leo31santi@gmail.com";
                }}
              >
                leo31santi@gmail.com
              </div>
              <div
                className="clickable footerMarginLink"
                onClick={() => {
                  window.open("https://vk.com/leosann");
                }}
              >
                vk.com/leosann
              </div>
            </div>
            <div className="footerText" id="social-networks">
              <div className="footerTitle">Соц. сети</div>

              <div className="socialLogo">
                <img
                  className="socialLogo clickable"
                  src={tgLogo}
                  alt="А где фотка"
                  onClick={() => {
                    window.open("https://t.me/phy6_tg");
                  }}
                ></img>
              </div>
              <div className="socialLogo">
                <img
                  className="socialLogo clickable"
                  src={vkLogo}
                  alt="Нет фотки("
                  onClick={() => {
                    window.open("https://vk.com/phy_6");
                  }}
                ></img>
              </div>
            </div>
          </div>
          <div id="copyrightText">© Phy6, 2021</div>
        </div>
      </div>
    );
  else
    return (
      <div
        style={{
          // marginTop: "64px",
          // height: "20%",
          background: "#191919",
        }}
      >
        <div id="footerFiller">
          <div id="footerGrid">
            <div className="footerText" id="links">
              <div className="footerTitle">Ссылки:</div>
              <div
                className="link"
                onClick={() => {
                  history.push("/licenseAgreement");
                }}
              >
                Пользовательское соглашение
              </div>
              {/* <div className="link">Обратная связь</div> */}
            </div>
            <div className="footerText" id="contacts">
              <div className="footerTitle">Контакты:</div>
              <div
                className="clickable"
                onClick={() => {
                  window.open("https://vk.com/leosann");
                }}
              >
                vk.com/leosann
              </div>
              <div
                className="clickable"
                onClick={() => {
                  window.location = "mailto:leo31santi@gmail.com";
                }}
              >
                leo31santi@gmail.com
              </div>
            </div>
          </div>
          <div id="mobileSocialNetworksContainer">
            <div className="footerText" id="social-networks">
              <div className="footerTitle">Соц. сети</div>
              <div id="footerLogoRows">
                <div className="socialLogo">
                  <img
                    className="socialLogo clickable"
                    src={vkLogo}
                    alt="Нет фотки("
                    onClick={() => {
                      window.open("https://vk.com/phy_6");
                    }}
                  ></img>
                </div>
                <div className="socialLogo">
                  <img
                    className="socialLogo clickable"
                    src={tgLogo}
                    alt="А где фотка"
                    onClick={() => {
                      window.open("https://t.me/phy6_tg");
                    }}
                  ></img>
                </div>
              </div>
            </div>
          </div>

          <div id="copyrightText">© Phy6, 2019-2021</div>
        </div>
      </div>
    );
};
