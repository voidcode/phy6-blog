import React, { useState, useEffect } from "react";
import { URL } from "../../globals";
import {
  Header,
  BlogItem,
  BigPost,
  Footer,
  BackButton,
} from "../../components";
import moment from "moment";
import { useParams, useHistory } from "react-router-dom";
import "./index.css";
import { notFound } from "../../globals/images";


export default () => {

  return (
    <div id="pageFiller">
      <Header selected="Пост" />
      <div id="mainFillerContainer">
        <div id="mainFiller">
        	<div id="postMainContainer">
            	<div id="title">
            	  404
            	</div>
              <div style={{ display: "flex", justifyContent: "center" }}>
                <img src={notFound} style={{objectFit: "cover", width: '40%',}} />
              </div>
            </div>
        </div>
        <BackButton />
      </div>
      <Footer />
    </div>
  );
};
