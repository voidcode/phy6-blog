import React, { useEffect, useState } from "react";
import "./index.css";
import moment from "moment";
import { useHistory } from "react-router-dom";
import { BlogItem, BigPost, Header } from "../../components/";
import { URL as url } from "../../globals";

const AdminPage = () => {
  const token = localStorage.getItem("token");
  const checkToken = (token) => {};
  const navigation = useHistory();
  const [posts, setPosts] = useState([]);
  const [starredPosts, setStarredPosts] = useState([]);
  const [count, setCount] = useState(0);
  const [loadMoreVisible, setLoadMoreVisible] = useState(false);

  useEffect(() => {
    checkToken(token);
    getPosts();
    getStarredPosts()
  }, []);

  const getPosts = async (category) => {
    const body = {
      category: category,
      first: 9,
      skip: count,
    };
    await fetch(url + "/get_posts", {
      method: "POST",
      body: JSON.stringify(body),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (!data) {
          return 0;
        }
        if (data.postsEnded) {
          // alert("Посты кончились(");
          setLoadMoreVisible(false);
        } else {
          setLoadMoreVisible(true);
        }
        setCount(count + data.length);
        setPosts(posts.concat(data));
      });
  };

  const getStarredPosts = async (category) => {
    const body = {
      category: category,
    };
    await fetch(url + "/get_starred_posts", {
      method: "POST",
      body: JSON.stringify(body),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (!data) {
          return 0;
        }
        setStarredPosts(data);
      });
  };

  return (
    <div className="pageFiller">
      <Header />
      <button
        onClick={() => {
          navigation.push("/admin/edit/new");
        }}
      >
        Добавить новый пост
      </button>
      <div id="mainFillerContainer">
        <div id="mainFiller">
          <div id="mainContainer">
            <div id="contentContainer">
              <div id="contentGrid">
                {starredPosts.length > 0 &&
                  starredPosts
                    .filter(
                      (item) => item["starred"]
                    )
                    .map((item) => {
                      return (
                        <BigPost
                          key={item["id"]}
                          imageUrl={
                            item['photo']
                          }
                          title={item["title"]}
                          annotation={
                            item["annotation"]
                          }
                          category={
                            item["category"]
                          }
                          date={moment(item["time_added"])
                                  .format("DD.MM.YYYY")
                          }
                          onClick={() => {
                            navigation.push(
                              `/admin/edit/${item["id"]}`
                            );
                          }}
                        />
                      );
                    })}
                {posts.length > 0 &&
                  posts
                    .filter(
                      (item) => !item["starred"]
                    )
                    .map((item) => (
                      <BlogItem
                        key={item["id"]}
                        imageUrl={
                            item["photo"]
                        }
                        title={
                          item["title"]
                        }
                        text={
                          item["annotation"]
                        }
                        category={
                          item["category"]
                        }
                        date={
                          item["time_added"]
                            ? moment(item["time_added"])
                                .format("DD.MM.YYYY")
                            :
                            "??.??.????"
                        }
                        onClick={() => {
                          navigation.push(
                            `/admin/edit/${item["id"]}`
                          );
                        }}
                      />
                    ))}
              </div>
            </div>
            {loadMoreVisible && (
              <div id="mainLoadMoreButtonContainer">
                <div
                  onClick={() => getPosts(undefined)}
                  id="mainLoadMoreButton"
                >
                  Больше!
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default AdminPage;
