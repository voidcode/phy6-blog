import React, { useState, useEffect } from "react";
import { URL as url } from "../../globals/index";
import { useParams } from "react-router-dom";
import { Header } from "../../components";
import "./index.css";
const EditPost = () => {
  // Functions
  const getBase64Image = (img) => {
    var canvas = document.createElement("canvas");
    console.log({img})
    canvas.width = img.naturalWidth;
    canvas.height = img.naturalHeight;
    var ctx = canvas.getContext("2d");
    ctx.drawImage(img, 0, 0);
    var dataURL = canvas.toDataURL("image/png");
    return dataURL;
  };

  const htmlUnparse = (text) => {
    if (text.slice(0, 3) == "<p>") {
      text = text.slice(3);
    }
    if (text.slice(text.length - 4) == "</p>") {
      text = text.slice(0, -4);
    }
    text = text.split("<br />").join("\n");
    text = text.split("</p><p>").join("\n\n");
    return text;
  };

  const htmlParse = (text) => {
    text = "<p>" + text.split("\n\n").join("</p><p>") + "</p>";
    text = text.split("\n").join("<br />");
    return text;
  };
  const sendPost = () => {
    let photo;
    if (document.getElementById("imagePreview") && !isRemoteFile) {
      photo = getBase64Image(document.getElementById("imagePreview"));
    }
    const token = localStorage.getItem("token");

    // console.log(htmlParse(content));
    // if (!token) {
    //   alert("Авторизуйтесь!");
    //   return 0;
    // }
    const data = {
      token: token,
      title: title,
      annotation: subtitle,
      content: htmlParse(content),
      photo: photo,
      starred: isStarred,
      category: category,
    };

    fetch(url + "/create_post", { method: "POST", body: JSON.stringify(data) })
      .then((res) => res.json())
      .then((data) => {
        alert((() => {
          if (data.result) {
            return "Успешно!"
          } else {
            if (data.error) {
              return data.error
            }
            return "Что-то пошло не так!"
          }
        })())
        console.log(data);
      });

    return 0;
  };

  const getPost = (id) => {
    const token = localStorage.getItem("token");

    // console.log(htmlParse(content));
    // if (!token) {
    //   alert("Авторизуйтесь!");
    //   return 0;
    // }
    const data = {
      token: token,
      id: id,
    };

    fetch(url + "/get_posts", { method: "POST", body: JSON.stringify(data) })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          setIsRemote(true);
          setTitle(data.title);
          setSubtitle(data.annotation);
          setContent(htmlUnparse(data.content));
          setFile(data.photo);
          setCategory(data.category);
          setIsStarred(data.starred);
        }
        console.log(data);
      });

    return 0;
  };

  const updatePost = () => {
    let photo;
    if (document.getElementById("imagePreview") && !isRemoteFile) {
      photo = getBase64Image(document.getElementById("imagePreview"));
    }
    const token = localStorage.getItem("token");


    console.log({photo})
    // console.log(htmlParse(content));
    // if (!token) {
    //   alert("Авторизуйтесь!");
    //   return 0;
    // }
    const data = {
      token: token,
      id: id,
      title: title,
      annotation: subtitle,
      content: htmlParse(content),
      photo: photo,
      starred: isStarred,
      category: category,
    };

    fetch(url + "/update_post", { method: "POST", body: JSON.stringify(data) })
      .then((res) => res.json())
      .then((data) => {
        alert(data.result);
        console.log(data);
      });

    return 0;
  };

  const [title, setTitle] = useState("");
  const [content, setContent] = useState("");
  const [subtitle, setSubtitle] = useState("");
  const [file, setFile] = useState();
  const [isRemoteFile, setIsRemote] = useState(true);
  const [category, setCategory] = useState("");
  const [isStarred, setIsStarred] = useState(false);

  let { id } = useParams();
  // id = id.slice(1);

  useEffect(() => {
    setTitle("");
    setContent("");
    setSubtitle("");
    setFile();
    setIsRemote(true);
    setCategory("");
    setIsStarred(false);
    switch (id) {
      case "new":
        break;
      default:
        getPost(id);
    }
  }, [id]);


  useEffect(() => {
    console.log({isStarred})
  }, [isStarred]);


  return (
    <div>
      <Header />
      <div>
        <div id="editPostTopContainer">
          <div id="editPostPreview">
            {file && <img id="imagePreview" src={file} />}
          </div>
          <div>
            <div className="editPostContRow">
              <div className="editPostText">Заголовок:</div>
              <input
                className="editPostInput"
                value={title}
                onChange={(event) => {
                  setTitle(event.target.value);
                }}
                maxLength={30}
              ></input>
              <div className="editPostText editPostTinyText editPostSymbolCounter">
                {title.length}/30
              </div>
            </div>
            <div className="editPostContRow">
              <div className="editPostText">Подзаголовок:</div>
              <input
                className="editPostInput"
                value={subtitle}
                onChange={(event) => {
                  setSubtitle(event.target.value);
                }}
                maxLength={80}
              ></input>
              <div className="editPostText tinyText">{subtitle.length}/80</div>
            </div>
            <div className="editPostContRow">
              <div className="editPostText">Категория:</div>
              <input
                value={category}
                onChange={(event) => {
                  setCategory(event.target.value);
                }}
              ></input>
            </div>
            <div>
              <div className="editPostText">Превью:</div>
              <input
                type="file"
                onChange={(event) => {
                  setIsRemote(false);
                  setFile(URL.createObjectURL(event.target.files[0]));
                }}
              ></input>
            </div>
          </div>
        </div>
        <div id="editPostBottomContainer">
          <div id="editPostTextareaContainer">
            <div className="editPostText">Контент:</div>
            <textarea
              id="editPostTextarea"
              value={content}
              onChange={(event) => {
                setContent(event.target.value);
              }}
            ></textarea>
          </div>
          <div id="" className="editPostText">
            Закрепленное:{" "}
            <input
              value={isStarred}
              checked={isStarred}
              type="checkbox"
              onChange={(event) => {
                setIsStarred(event.target.checked);
              }}
            ></input>
          </div>
          <button
            id="editPostSubmit"
            className={"editPostText"}
            onClick={() => {
              id === "new" ? sendPost() : updatePost();
            }}
          >
            Разместить пост!
          </button>
        </div>
      </div>
    </div>
  );

  return (
    <div>
      <div>{id === "new" ? "Добавить новый пост" : "Отредактировать пост"}</div>
    </div>
  );
};

export default EditPost;
