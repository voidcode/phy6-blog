import React, { useEffect, useState } from "react";
import { Header, BlogItem, BigPost, Footer } from "../../components";
import { URL } from "../../globals";
import moment from "moment";
import { useHistory, useParams } from "react-router-dom";
import "./index.css";
// import firebase from "firebase";
// import "firebase/storage";

// Get a reference to the storage service, which is used to create references in your storage bucket

export default ({ title = "Главная", category, ...props }) => {
  const navigation = useHistory();
  const [posts, setPosts] = useState([]);
  const [starredPosts, setStarredPosts] = useState([]);
  const [count, setCount] = useState(0);
  const [loadMoreVisible, setLoadMoreVisible] = useState(false);
  let scrollPosition = 0;

  if (!category && title != "Главная") {
    category = useParams().category;
    // category = category.slice(1);
  }

  useEffect(() => {
    let cachePosts = sessionStorage.getItem("posts");
    console.log({cachePosts})
    let cacheCount = sessionStorage.getItem("count");
    let cacheIsEnded = sessionStorage.getItem("isEnded") === "true";

    if (typeof cachePosts == "string") {
      cachePosts = undefined
    }

    cachePosts = cachePosts === "undefined" ? undefined : cachePosts;
    cacheCount = cacheCount === "undefined" ? undefined : cacheCount;

    let oldCategory = sessionStorage.getItem("category");
    oldCategory = oldCategory === "undefined" ? undefined : oldCategory;

    if (!cachePosts || oldCategory !== category) {
      getPosts(category);
    } else {
      setPosts(JSON.parse(cachePosts));
      setCount(cacheCount);
      // console.log({ cacheIsEnded });
      setLoadMoreVisible(!cacheIsEnded);
    }

    let cacheStarredPosts = sessionStorage.getItem("starredPosts");
    cacheStarredPosts =
      cacheStarredPosts == "undefined" ? undefined : cacheStarredPosts;

    if (typeof cacheStarredPosts == "string") {
      cacheStarredPosts = undefined
    }



    if (!cacheStarredPosts || oldCategory != category) {
      getStarredPosts(category);
    } else {
      setStarredPosts(JSON.parse(cacheStarredPosts));
    }

    sessionStorage.setItem("category", category);

    let cachedScroll = sessionStorage.getItem("scrollPosition");
    cachedScroll = cachedScroll == "undefined" ? undefined : cachedScroll;

    if (cachedScroll && oldCategory == category) {
      let filler = document.getElementById("pageFiller");
      setTimeout(() => {
        filler.scrollTo(0, cachedScroll);
      }, 2);
    }
  }, []);

  const getStarredPosts = async (category) => {
    const body = {
      category: category,
    };
    await fetch(URL + "/get_starred_posts", {
      method: "POST",
      body: JSON.stringify(body),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (!data) {
          return 0;
        }
        sessionStorage.setItem("starredPosts", JSON.stringify(data));
        setStarredPosts(data);
      });
  };

  const getPosts = async (category) => {
    const body = {
      category: category,
      first: 9,
      skip: count,
    };
    console.log({body})
    await fetch(URL + "/get_posts", {
      method: "POST",
      body: JSON.stringify(body),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (!data) {
          return 0;
        }
        if (posts.length > 0) {
          if (data[data.length - 1].id == posts[data.length - 1].id) {
            setLoadMoreVisible(false);
          } else {
            setLoadMoreVisible(true);
          }
        }

        sessionStorage.setItem(
          "posts",
          JSON.stringify(posts.concat(data))
        );
        sessionStorage.setItem("count", count + data.length);
        sessionStorage.setItem("isEnded", data.postsEnded);
        setCount(count + data.length);
        setPosts(posts.concat(data));
      });
  };

  return (
    <div
      id="pageFiller"
      onScroll={(event) => {
        scrollPosition = event.target.scrollTop;
      }}
    >
      <Header selected={category || title} />
      {/* Body */}
      <div style={{display: 'none'}}>Hello! It's me.</div>
      <div id="mainFillerContainer">
        <div id="mainFiller">
          <div id="mainContainer">
            <div id="header">{category || title}</div>
            <div id="contentContainer">
              <div id="contentGrid">
                {starredPosts.length > 0 &&
                  starredPosts
                    .filter(
                      (item) => item["starred"]
                    )
                    .map((item) => {
                      const fields = item;
                      const id = item["id"];
                      const imageUrl = item["photo"];
                      const title = fields["title"];
                      const annotation = fields["annotation"];
                      const category = fields["category"];
                        const date = item["time_added"]
                            ? moment(item["time_added"])
                                .format("DD.MM.YYYY")
                            :
                            "??.??.????"

                      return (
                        <BigPost
                          key={id}
                          {...{ imageUrl, title, annotation, category, date }}
                          onClick={() => {
                            navigation.push(`/post/${id}`);
                            sessionStorage.setItem(
                              "scrollPosition",
                              scrollPosition
                            );
                          }}
                        />
                      );
                    })}
                {posts.length > 0 &&
                  posts
                    .filter(
                      (item) => { console.log(1); return !item["starred"]}
                    )
                    .map((item, index) => {
                      const id = item["id"];
                      const imageUrl = item["photo"];
                      const title = item["title"];
                      const text = item["annotation"];
                      const category = item["category"];
                      const date = item["time_added"]
                          ? moment(item["time_added"])
                              .format("DD.MM.YYYY")
                          :
                          "??.??.????"
                      return (
                        <BlogItem
                          key={id}
                          {...{
                            imageUrl,
                            title,
                            text,
                            category,
                            date,
                          }}
                          onClick={() => {
                            navigation.push(`/post/${id}`);
                            sessionStorage.setItem(
                              "scrollPosition",
                              scrollPosition
                            );
                          }}
                        />
                      );
                    })}
              </div>
            </div>
            {loadMoreVisible && (
              <div id="mainLoadMoreButtonContainer">
                <div onClick={() => getPosts(category)} id="mainLoadMoreButton">
                  Больше!
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
};
