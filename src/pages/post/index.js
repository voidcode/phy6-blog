import React, { useState, useEffect } from "react";
import { URL } from "../../globals";
import {
  Header,
  BlogItem,
  BigPost,
  Footer,
  BackButton,
} from "../../components";
import moment from "moment";
import { useParams, useHistory } from "react-router-dom";
import "./index.css";

export default () => {
  const loadData = () =>
    getPost(id, (data) => {
      setData(data);
      if (data.recommendations) {
        console.log(data.recommendations);
        data.recommendations.values.map((item) => {
          getPost(item, (data) => {
            data.id = item;
            setRecommendations(recommendations.concat(data));
          });
        });
      }
    });

  const getPost = async (id, callback) => {
    const body = { id: id };

    await fetch(URL + "/get_posts", {
      method: "POST",
      body: JSON.stringify(body),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (!data["content"]) {
          navigation.push("/404")
        }
        callback(data);
      });
  };

  const defaultData = {
    title: "Этому заголовку так одиноко...",
    content: "Здесь пока ничего нет(",
  };

  const [data, setData] = useState();

  const [recommendations, setRecommendations] = useState([]);

  let navigation = useHistory();
  let { id } = useParams();
  // id = id.slice(1);

  useEffect(() => {
    setData(defaultData);
    setRecommendations([]);
    loadData();
  }, [id]);


  return (
    <div id="pageFiller">
      <Header selected="Пост" />
      <div id="mainFillerContainer">
        <div id="mainFiller">
          <div id="postMainContainer">
            <div id="title">
              { data && data.title }
            </div>
            <BigPost
              imageUrl={
                data && data["photo"]
              }
              annotation={
                data && data.annotation && data.annotation
              }
              category={data && data.category && data.category}
              date={
                data &&
                moment(data["time_added"])
                  .format("DD.MM.YYYY")
              }
            />
            <div className="boxContainer" id="dataContainer">
              <div
                id="textBox"
                dangerouslySetInnerHTML={
                  data &&
                  data.content && {
                    __html: data.content,
                  }
                }
              ></div>
            </div>
            <div id="postBottomPanel">
              {recommendations.length > 0 && (
                <div id="recommendationHalf">
                  <div id="alsoRecommend">Рекомендуем также</div>
                  <div id="recommendationRow">
                    {recommendations.map((item, index) => (
                      <BlogItem
                        key={index}
                        className="recommendationBlogItem"
                        category={item.category}
                        title={item.title}
                        imageUrl={item.photo}
                        onClick={() => {
                          navigation.push("/post/" + item.id);
                        }}
                        date={
                          item["time_added"]
                            ? moment(item["time_added"])
                                .format("DD.MM.YYYY")
                            : "??.??.????"
                        }
                      />
                    ))}
                  </div>
                </div>
              )}
            </div>
          </div>
        </div>
        <BackButton />
      </div>
      <Footer />
    </div>
  );
};
