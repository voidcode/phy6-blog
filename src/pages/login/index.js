import React, { useState } from "react";
import { URL } from "../../globals";

export default () => {
  // const [login, setLogin] = useState("");
  // const [password, setPassword] = useState("");

  const [token, setToken] = useState("token")

  const authenticate = () => {
    // // const body = { login, password };
    // const bod
    // fetch(URL + "/login", {
    //   method: "POST",
    //   body: JSON.stringify(body),
    // })
    //   .then((res) => res.json())
    //   .then((data) => {
    //     console.log(data);
    //     if (data.error) {
    //       console.log(data.error);
    //     } else {
    //       if (data.token) {
    //         localStorage.setItem("token", data.token);
    //       }
    //     }
    //   });


    localStorage.setItem("token", token)
  };

  return (
    <div id="pageFiller">
      Ваш токен: {localStorage.getItem("token")} <br></br>
      <input
        placeholder="Токен"
        id="token"
        onChange={(event) => setToken(event.target.value)}
      />
      {
              // <input
      //   placeholder="Пароль"
      //   id="password"
      //   onChange={(event) => setPassword(event.target.value)}
      // />
      }

      <button onClick={() => authenticate()}>Войти</button>
    </div>
  );
};
