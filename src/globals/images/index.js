import cat from "./cat/cat@2x.png";
import tgLogo from "./tgLogo/tgLogo@2x.png";
import vkLogo from "./vkLogo/vkLogo@2x.png";
import menu from "./menu/menu@2x.png";
import arrowBack from "./arrowBack/arrowBack@2x.png";
import arrowBackDesktop from "./arrowBack-desktop/Arrow 1@2x.png";
import agreement from "./agreement/agreement.jpg";
import notFound from "./404/404.png"
export { cat, vkLogo, tgLogo, arrowBack, menu, arrowBackDesktop, agreement, notFound };
