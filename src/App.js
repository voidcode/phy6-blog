import React from "react";
import {
  HashRouter as Router,
  Switch,
  Route,
  useHistory,
  Redirect,
} from "react-router-dom";
import Home from "./pages/main";
import Post from "./pages/post";
import Screen404 from './pages/404'
import AboutUs from "./pages/aboutUs";
import Login from "./pages/login";
import LicenseAgreement from "./pages/licenseAgreement";
import "./index.css";
import AdminPage from "./pages/adminPage/index";
import EditPost from "./pages/editPost/index";

function App() {
  let history = useHistory();
  return (
    <Router history={history} forceRefresh={false}>
      <Switch>
        <Route
          key="main"
          exact
          path={["/"]}
          render={(props) => <Home {...props} />}
        ></Route>
        <Route
          key="memes"
          exact
          path={["/memes"]}
          render={(props) => <Home {...props} category="Мемы" />}
        ></Route>
        <Route
          key="blog"
          exact
          path={["/blog"]}
          render={(props) => <Home {...props} category="Блог" />}
        ></Route>
        <Route
          key="category"
          exact
          path={"/category/:category"}
          render={(props) => <Home {...props} title="Категория" />}
        ></Route>
        <Route
          exact
          path="/post/:id"
          render={(props) => <Post {...props} />}
        ></Route>
        <Route exact path="/aboutUs">
          <AboutUs category="О нас" />
        </Route>
        <Route exact path="/login">
          <Login />
        </Route>
        <Route exact path="/licenseAgreement">
          <LicenseAgreement />
        </Route>
        <Route exact path="/admin">
          <AdminPage />
        </Route>
        <Route exact path="/admin/edit/:id">
          <EditPost />
        </Route>
        <Route exact path="/404">
          <Screen404 />
        </Route>
        <Route>
        		<Screen404 />
      	</Route>
      </Switch>
    </Router>
  );
}

export default App;
